package com.smess.basemvpandroidarchitecture.workers;

/**
 * Created by samy.messara
 */

public interface BaseWorkerCallback {
    void onWorkerError(String error, String tag);
    void onTokenInvalid();
}
