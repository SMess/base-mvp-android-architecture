package com.smess.basemvpandroidarchitecture.workers



import com.smess.basemvpandroidarchitecture.executor.Executor
import com.smess.basemvpandroidarchitecture.executor.MainThread
import com.smess.basemvpandroidarchitecture.executor.di.ExecutorModule
import com.smess.basemvpandroidarchitecture.workers.di.DaggerWorkerComponent
import javax.inject.Inject

/**
 * Abstract interactor: base class for interactors
 *
 * Created by samy.messara
 */
abstract class AbstractWorker {

    var mThreadExecutor: Executor? = null
        @Inject set

    var mMainThread: MainThread? = null
        @Inject set

    @Volatile
    protected var mIsCanceled: Boolean = false
    @Volatile
    var isRunning: Boolean = false

    init {
        DaggerWorkerComponent
                .builder()
                .executorModule(ExecutorModule())
                .build()
                .inject(this)
    }

    /**
     * This method contains the actual business logic of the interactor. It SHOULD NOT BE USED DIRECTLY but, instead, a
     * developer should call the execute() method of an interactor to make sure the operation is done on a background thread.
     *
     *
     * This method should only be called directly while doing unit/integration tests. That is the only reason it is declared
     * public as to help with easier testing.
     */
    abstract fun run()

    fun cancel() {
        mIsCanceled = true
        isRunning = false
    }

    fun onFinished() {
        isRunning = false
        mIsCanceled = false
    }

    fun execute() {

        // mark this interactor as running
        this.isRunning = true

        // start running this interactor in a background thread
        mThreadExecutor!!.execute(this)
    }

}
