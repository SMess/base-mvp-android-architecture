package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * Created by samy.messara
 */
abstract class BaseInteractor <T : MVPContract.PresenterContract>(protected var presenter: T) : MVPContract.InteractorContract {

    protected  var appContext: Context? = null

    /**
     * Bound to its presenter's onViewCreated
     */
    override fun onCreate(savedInstanceState: Bundle?) {}

    override fun onCreate(savedInstanceState: Bundle?, context: Context) {
        this.appContext = context
        onCreate(savedInstanceState)
    }

    /**
     * Bound to its presenter's onStart
     */
    override fun onStart() {}

    /**
     * Bound to its presenter's onNewIntent
     */
    override fun onNewIntent(intent: Intent) {}

    /**
     * Bound to its presenter's onPause
     */
    override fun onPause() {}

    /**
     * Bound to its presenter's onResume
     */
    override fun onResume() {}

    /**
     * Bound to its presenter's onStop
     */
    override fun onStop() {}

    /**
     * Bound to its presenter's onDestroyView
     */
    override fun onDestroy() {}
}