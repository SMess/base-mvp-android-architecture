package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * Created by samy.messara
 */

abstract class BasePresenter<T : MVPContract.ViewContract, I : MVPContract.InteractorContract>(protected var view: T) : MVPContract.PresenterContract {


    protected lateinit var interactor: I
    protected lateinit var appContext: Context

    init {
        getInteractorInstance()
    }

    /**
     * Bound to its view's onViewCreated
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        appContext = view.getViewAppContext()
        getInteractorInstance().onCreate(savedInstanceState, appContext)
    }

    /**
     * Bound to its view's onStart
     */
    override fun onStart() {
        getInteractorInstance().onStart()
    }

    /**
     * Bound to its view's onNewIntent
     */
    override fun onNewIntent(intent: Intent) {
        getInteractorInstance().onNewIntent(intent)
    }

    /**
     * Bound to its view's onPause
     */
    override fun onPause() {
        getInteractorInstance().onPause()
    }

    /**
     * Bound to its view's onResume
     */
    override fun onResume() {
        getInteractorInstance().onPause()
    }

    /**
     * Bound to its view's onStop
     */
    override fun onStop() {
        getInteractorInstance().onStop()
    }

    /**
     * Bound to its view's onDestroyView
     */
    override fun onDestroy() {
        getInteractorInstance().onDestroy()
    }

    /**
     * Bound to its view's setBundle
     */
    override fun setBundle(arguments: Bundle?) {}

    /**
     * Bound to its view's setIntent
     */
    override fun setIntent(intent: Intent?) {}

    /**
     * Getter on the view's Presenter
     *
     * @return a bounded Presenter that extends MVPPresenter
     */
    fun getInteractorInstance(): I {
        interactor = createInteractor(this)
        return interactor
    }


    /**
     * Allow to expose the view's Presenter
     * For example:
     * protected MyPresenter createPresenter() {
     * return new MyPresenter(this);
     * }
     *
     * @return the Presenter we want to bind to this view (must extends MVPPresenter)
     */
    protected abstract fun createInteractor(P: BasePresenter<T, I>): I
}