package com.smess.basemvpandroidarchitecture.executor.impl

import com.smess.basemvpandroidarchitecture.executor.Executor
import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * This singleton class will make sure that each interactor operation gets a background thread.
 *
 * Created by samy.messara
 */

class ThreadExecutor : Executor {

    private val mThreadPoolExecutor: ThreadPoolExecutor

    init {
        val keepAlive = KEEP_ALIVE_TIME.toLong()
        mThreadPoolExecutor = ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                keepAlive,
                TIME_UNIT,
                WORK_QUEUE)
    }

    override fun execute(interactor: AbstractWorker) {
        mThreadPoolExecutor.submit {
            // run the main logic
            interactor.run()

            // mark it as finished
            interactor.onFinished()
        }
    }

    companion object {

        // This is a singleton
        @Volatile
        private var sThreadExecutor: ThreadExecutor? = null

        private const val CORE_POOL_SIZE = 10
        private const val MAX_POOL_SIZE = 12
        private const val KEEP_ALIVE_TIME = 120
        private val TIME_UNIT = TimeUnit.SECONDS
        private val WORK_QUEUE = LinkedBlockingQueue<Runnable>()

        /**
         * Returns a singleton instance of this executor. If the executor is not initialized then it initializes it and returns
         * the instance.
         */
        val instance: Executor?
            get() {
                if (sThreadExecutor == null) {
                    sThreadExecutor = ThreadExecutor()
                }

                return sThreadExecutor
            }
    }
}
