**MVP stands for Model View Presenter.**

The Model is where you define your data beans and other classes that hold the data.

The View is responsible of rendering views you will be able to interact with. Usually, all the pages or screen you can see across an application belongs to the View module.

The Presenter is the business logic of the application. As opposed to the MVC Pattern, it is not a master controller responsible to handle the business logic and routing to different view. Each view owns a presenter.

In terms of advantages, it brings:


- clear separated code between business logic and UI logic. Code is less complex.
- Encapsulated code, implying more **modularity** and **reusability**